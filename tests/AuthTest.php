<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthTest extends TestCase
{
    public function testRegistrationSuccess()
    {
        $registerResponse = $this->post(route('user.registration'), [
            'first_name'            => 'Test',
            'last_name'             => 'Test',
            'phone'                 => '+380000000000',
            'email'                 => 'test@gmail.com',
            'password'              => 'dasjdkjasdaskmkljqw',
            'password_confirmation' => 'dasjdkjasdaskmkljqw',
        ]);

        User::where('email','test@gmail.com')->delete();

        $registerResponse->assertResponseStatus(201);
    }

    public function testLoginSuccess()
    {
        $userTest = App\Models\User::factory()->create([
            'first_name' => 'Test',
            'last_name'  => 'Test',
            'phone'      => '+380000000000',
            'email'      => 'test@gmail.com',
            'password'   => Hash::make('dasjdkjasdaskmkljqw'),
        ]);

        $loginResponse = $this->post(route('user.login'), [
            'email' => $userTest->email,
            'password' => 'dasjdkjasdaskmkljqw',
        ]);

        User::where('email', $userTest->email)->delete();

        $loginResponse->assertResponseOk();

    }

    public function testLoginFailedData()
    {
        $response = $this->post(route('user.login'), [
            'email' => 'rendom@gmail.com',
            'password'=> 'esaesa'
        ]);
        $response->assertResponseStatus(422);
    }
}
