<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

use Laravel\Lumen\Routing\Router;

$router->group([
    'prefix' => 'api'
], function ()  use ($router) {

    $router->group([
        'namespace' => 'User',
        'prefix' => 'user',
        'as' => 'user'
    ], function () use ($router) {

        $router->post('register', [
            'as' => 'registration',
            'uses' => 'AuthController@registration'
        ]);

        $router->post('sign-in', [
            'as' => 'login',
            'uses' => 'AuthController@login'
        ]);

        $router->post('recover-password', [
            'as' => 'recover-password',
            'uses' => 'AuthController@recoverPassword'
        ]);

        $router->group([
            'prefix' => 'companies',
            'middleware' => 'auth',
            'as' => 'company'
        ], function () use ($router) {

            $router->get('/', [
                'as'   => 'index',
                'uses' => 'CompanyController@index'
            ]);

            $router->post('/', [
                'as'   => 'store',
                'uses' => 'CompanyController@store'
            ]);

        });

    });

});
