```
+------+----------------------------+-----------------------+---------------------------------------------+-----------------+------------+
| Verb | Path                       | NamedRoute            | Controller                                  | Action          | Middleware |
+------+----------------------------+-----------------------+---------------------------------------------+-----------------+------------+
| GET  | /                          |                       | None                                        | Closure         |            |
| POST | /api/user/register         | user.registration     | App\Http\Controllers\User\AuthController    | registration    |            |
| POST | /api/user/sign-in          | user.login            | App\Http\Controllers\User\AuthController    | login           |            |
| POST | /api/user/recover-password | user.recover-password | App\Http\Controllers\User\AuthController    | recoverPassword |            |
| GET  | /api/user                  | user.index            | App\Http\Controllers\User\CompanyController | index           | auth       |
| POST | /api/user                  | user.store            | App\Http\Controllers\User\CompanyController | store           | auth       |
+------+----------------------------+-----------------------+---------------------------------------------+-----------------+------------+
```

