<?php


namespace App\Repositories\User\Company;


use Illuminate\Http\Request;

interface ICompanyRepository
{
    public function paginateWithAuthUser();

    public function store(Request $request);
}
