<?php


namespace App\Repositories\User\Company;


use App\Models\Company;
use Illuminate\Http\Request;

class CompanyRepository implements ICompanyRepository
{

    const DEFAULT_COUNT_PAGINATE = 5;

    /**
     * Paginate authorization user companies
     *
     * @return mixed
     */
    public function paginateWithAuthUser()
    {
        return auth()
            ->user()
            ->company()
            ->paginate(self::DEFAULT_COUNT_PAGINATE);
    }

    /**
     * @param Request $request
     * @return Company
     */
    public function store(Request $request): Company
    {
        return Company::create(
            $this->prepareBeforeStoreCompanyData($request->all())
        );
    }

    /**
     * @param array $data
     * @return array
     */
    private function prepareBeforeStoreCompanyData(array $data): array
    {
        $data['user_id'] = auth()->id();

        return $data;
    }
}
