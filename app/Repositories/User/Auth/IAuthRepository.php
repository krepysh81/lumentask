<?php


namespace App\Repositories\User\Auth;


use Illuminate\Http\Request;

interface IAuthRepository
{
    public function login(Request $request);

    public function registration(Request $request);

    public function recoverPassword(Request $request);
}
