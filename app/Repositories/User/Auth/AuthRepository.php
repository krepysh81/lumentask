<?php


namespace App\Repositories\User\Auth;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthRepository implements IAuthRepository
{
    const SALT_RESET_TOKEN = 'hellotest';

    /**
     * @param Request $request
     * @return User
     */
    public function login(Request $request): ?User
    {
        $user = $this->findUserByEmail($request->email);
        if (!$user) {
            return null;
        }

        if (!Hash::check($request->password, $user->password)) {
            return null;
        }

        $this->updateUser(
            $user,
            $this->prepareUserToken()
        );

        return $user->fresh();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function registration(Request $request): User
    {
        return $this->storeUser($request->all());
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function recoverPassword(Request $request): bool
    {
        $user = $this->findUserByEmail($request->email);
        if (!$user) {
            return false;
        }

        return $this->updateUser(
            $user,
            $this->prepareUserResetToken($request->email)
        );

    }

    /**
     * @param array $data
     * @return User
     */
    private function storeUser(array $data): User
    {
        return User::create($this->prepareBeforeStoreUserData($data));
    }

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
    private function updateUser(User $user, array $data): bool
    {
        return $user->update(
            $data
        );
    }

    /**
     * @param array $data
     * @return array
     */
    private function prepareBeforeStoreUserData(array $data): array
    {
        $data['password'] = Hash::make($data['password']);

        return $data;
    }

    /**
     * @return array
     */
    private function prepareUserToken(): array
    {
        return [
            'api_token' => base64_encode(Str::random(40))
        ];
    }

    /**
     * @param string $email
     * @return array
     */
    private function prepareUserResetToken(string $email): array
    {
        return [
            'password_reset_token' => Hash::make(
                self::SALT_RESET_TOKEN.$email
            )
        ];
    }

    /**
     * @param string $email
     * @return mixed
     */
    private function findUserByEmail(string $email)
    {
        return User::where('email', $email)->first();
    }
}
