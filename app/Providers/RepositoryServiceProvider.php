<?php

namespace App\Providers;

use App\Repositories\User\Auth\AuthRepository;
use App\Repositories\User\Auth\IAuthRepository;
use App\Repositories\User\Company\CompanyRepository;
use App\Repositories\User\Company\ICompanyRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        $this->app->bind(
            IAuthRepository::class,
            AuthRepository::class
        );

        $this->app->bind(
            ICompanyRepository::class,
            CompanyRepository::class
        );
    }
}
