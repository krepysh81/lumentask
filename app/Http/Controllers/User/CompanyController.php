<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\Company\CompanyResource;
use App\Repositories\User\Company\ICompanyRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CompanyController extends Controller
{
    /**
     * @var ICompanyRepository
     */
    private $companyRepository;

    /**
     * CompanyController constructor.
     * @param ICompanyRepository $ICompanyRepository
     */
    public function __construct(ICompanyRepository $ICompanyRepository)
    {
        $this->companyRepository = $ICompanyRepository;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CompanyResource::collection(
            $this->companyRepository->paginateWithAuthUser()
        );
    }

    /**
     * @param Request $request
     * @return CompanyResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => ['required', 'string', 'min:8','max:255'],
            'phone'       => [
                'required',
                'string',
                'unique:companies',
            ],
            'description' => ['nullable']
        ]);

        $company = $this->companyRepository->store($request);

        return CompanyResource::make($company);
    }
}
