<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Repositories\User\Auth\IAuthRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * @var IAuthRepository
     */
    private $authRepository;

    /**
     * AuthController constructor.
     * @param IAuthRepository $IAuthRepository
     */
    public function __construct(IAuthRepository $IAuthRepository)
    {
        $this->authRepository = $IAuthRepository;
    }

    /**
     * @param Request $request
     * @return UserResource|JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => ['required', 'string', 'email'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        $user = $this->authRepository->login($request);

        if (!$user) {
            return response()->json([
                'message' => __('custom.login_fail'),
            ], 400);
        }

        return UserResource::make($user)
            ->additional([
                'api_token' => $user->api_token
            ]);
    }

    /**
     * @param Request $request
     * @return UserResource
     * @throws ValidationException
     */
    public function registration(Request $request): UserResource
    {
        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name'  => ['required', 'string', 'max:255'],
            'email'      => ['required', 'string', 'email', 'unique:users'],
            'phone'      => ['required', 'string', 'unique:users'],
            'password'   => ['required', 'string', 'min:8', 'confirmed']
        ]);

        $user = $this->authRepository->registration($request);

        return UserResource::make($user);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function recoverPassword(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'string', 'email'],
        ]);

        $result = $this->authRepository->recoverPassword($request);

        if (!$result) {
            return response()->json([
                'message' => __('custom.password_reset_error'),
            ], 400);
        }

        return response()->json([
            'message' => __('custom.password_reset_success'),
        ]);
    }

}
